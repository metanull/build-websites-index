<?php
require_once('vendor/autoload.php');
use Spatie\Image\Manipulations;
use Spatie\Browsershot\Browsershot;
use Symfony\Component\HttpClient\HttpClient;

if($argc != 3) {
        throw new \Exception("Missing argument! Syntax: php ${argv[0]} 'URL' 'FILENAME.png'");
}
$url = $argv[1];
$filename = $argv[2];

$client = HttpClient::create();
$statusCode = 500;
try {
	$response = $client->request('GET', $url, ['timeout' => 5, 'max_redirects' => 0]);
	$statusCode = $response->getStatusCode();
} catch(\Exception $e) {
	$statusCode = 404;
}
switch((int)$statusCode) {
	case 200: {
		BrowserShot::url($url)
			->waitUntilNetworkIdle()
			->windowSize(1080,1024)
			->fit(Manipulations::FIT_CONTAIN, 400,300)
			->save($filename);
		echo "${url} : Ok";
#		usleep(100);
		exit(0);
	} break;
	case 302: {
		$redirectingTo = $response->getInfo('redirect_url');
		echo "${url} : HTTP ${statusCode}, Redirecting to: ${redirectingTo}";
		Browsershot::html("<h1>REDIRECTION</h1><h3>Target: ${redirectingTo}</h3>")
			->windowSize(400,300)
			->save($filename);
	} break;
	default: {
		echo "${url} : HTTP ${statusCode}";
		Browsershot::html("<h1>HTTP ERROR</h1><h3>HTTP ${statusCode}</h3>")
			->windowSize(400,300)
			->save($filename);
	} break;
}
# usleep(100);
if(!$statusCode) {
	exit(1);
}
exit($statusCode);