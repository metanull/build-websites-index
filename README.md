# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Requirements ###

* [Git](https://git-scm.com/)
* [Composer](https://getcomposer.org/)
* [Node.js](https://nodejs.org/)

### Installation ###

```
git clone https://metanull@bitbucket.org/metanull/build-websites-index.git
cd build-websites-index
composer install
npm install
```

### Usage ###

Take a screenshot of a website:
`php web-screenshot.php http://www.example.com example_com.png`


### Automating ###

Take a look to `build-index.sh.example`, it demonstrates one way of automating the process through a bash shell script